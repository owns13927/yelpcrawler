"""
Elias Wood (owns13927@yahoo.com)
2015-04-20
a web crawler for yelp.
"""

import requests
import logging
logging.getLogger('requests').setLevel(logging.WARNING)

from bs4 import BeautifulSoup

from myloggingbase import MyLoggingBase

class YelpWebCrawler(MyLoggingBase):
    """
    the first attempt.  future version will use more generic flow/methods and
    easily/generically recover (needs to function like yelpapicrawler...)
    things to do:
        1. handle if url doesn't load
        2. grab more info from store's yelp page
        3. move searched fields into get_default_headers
        4. what about searching by zip???
    """
    __version__ = '0.0.5'
    
    _session = None
    _site_domain = 'http://www.yelp.com'
    _base_url = _site_domain + \
        '/search?l=p:{state}:{city}::&start={offset}&cflt={category}&attrs={attrs}'
    #{attrs} = {feature},{price}'
    
    MAX_STORE_COUNT = 10
    MAX_PAGE_COUNT = 100
    MAX_STORE_COUNT = 1000
    
    _total_stores = 0
    _total_stores_missed = 0
    _stores_outside_zip = 0
    
    def __init__(self):
        MyLoggingBase.__init__(self)
        
    def init_session(self):
        self._session = requests.Session()
    
    def get_default_headers(self):
        return ('total','offset',
                'title','yelp_url','phone','rating','rating_count','price',
                'categories','neighborhoods',
                'address','city','state_code','postal_code','country_code',
                'extracted_dt')
    
    #===========================================================================
    # what most should call - how to start a search
    #===========================================================================
    def iter_search(self,state,city,category,attrs):
        """
        alcohol is a feature
        attrs can be a comma separated str
        iterates results as a dict
        NOTE: this method would ideal check params and be more friendly, like
              have options for going through each subcat,ignoring attrs, etc
        """
        if self._session is None: self.init_session()
        
        return self.iter_pages(state=state,
                               city=city,
                               category=category,
                               attrs=attrs)
        
    #===========================================================================
    # Iterate through the pages of a search
    #===========================================================================
    def iter_pages(self,**keys):
        """
        'total','offset','extracted_dt'
        """
        # set params & counters
        keys.setdefault('offset',0)
        page_counter = 0
        
        d=dict(total=None,offset=keys['offset'])
        
        # start cycling through the pages!
        while True:
            # pull the page!
            d['extracted_dt'] = self.get_current_timestamp()
            soup = self._get_soup_from_search(**keys)
            page_counter += 1
            
            # set the total if not yet set!
            if d['total'] is None:
                a = soup.find('span',{'class':'pagination-results-window'})
                if a is None:
                    # there are no results for this search!
                    total = 0
                    break
                    
                s = a.get_text(strip=True)
                d['total'] = s[s.rfind(' ')+1:]
                total = int(d['total'])
                self._total_stores_missed += total
                self.logger.info('%s: %d results found...',keys,total)
            
            # iter through each store!    
            for store in self.iter_stores(soup):
                # track metrics
                self._total_stores += 1
                self._total_stores_missed -= 1
                # update dict with the page level info
                store.update(d)
                # yield the data!
                yield store
                # increment offset (so csv is right)
                d['offset'] += 1
            
            # let the user know I'm still working...
            if page_counter%5==0: self.logger.info('%d stores, %d pages',
                                                   d['offset'],page_counter)
            
            if d['offset'] >= total or \
               d['offset'] >= self.MAX_STORE_COUNT or \
               page_counter >= self.MAX_PAGE_COUNT:
                break # all done paging
            else:
                keys['offset'] = d['offset']
        # inform the user
        self.logger.info('%d stores, %d pages, %d stores missed.',
                         d['offset'],page_counter,total-d['offset'])
        
        
    
    #===========================================================================
    # Iterate through the stores on a page
    #===========================================================================
    def iter_stores(self,soup_page):
        """
      'title','yelp_url','phone','rating','rating_count','price',
      'categories','neighborhoods',
      'address','city','state_code','postal_code','country_code',
        """
        for soup in soup_page.find_all('div',{'class':'search-result natural-search-result'}):
            # str(store.get('data-key')) == d['offset']+1
            d = dict()
            #===================================================================
            # title,url
            #===================================================================
            a = self._try_find(soup,'a',{'class':'biz-name'},use_get=False)
            if a=='': d['title'] = d['yelp_url'] = '(unknown)'
            else:
                d['title'] = a.get_text(strip=True)
                d['yelp_url'] = self._site_domain+a.get('href')
            del a
            
            #===================================================================
            # get 2nd level info
            #===================================================================
            d.update(self.get_store_details(d['yelp_url']))
            
            #===================================================================
            # phone
            #===================================================================
            d['phone'] = soup.find('span',{'class':'biz-phone'}).get_text(strip=True)
            #===================================================================
            # rating
            #===================================================================
            a = self._try_find(soup,'div',{'class':'rating-large'},use_get=False)
            if a=='': d['rating'] = 'N/A'
            else:
                s = a.i.get('title')
                d['rating'] = s[:s.find(' ')]
                del s
            del a
            #===================================================================
            # rating_count
            #===================================================================
            a = self._try_find(soup,'span',{'class':'review-count rating-qualifier'},strip=True)
            d['rating_count'] = a[:a.find(' ')]
            del a
            #===================================================================
            # price
            #===================================================================
            d['price'] = self._try_find(soup,'span',
                                        {'class':'business-attribute price-range'},
                                        strip=True).count('$')
            #===================================================================
            # categories (csv)
            #===================================================================
            a = self._try_find(soup,'span',{'class':'category-str-list'},use_get=False)
            d['categories'] = '' if a=='' else a.get_text(strip=True)
            del a
            #===================================================================
            # neighborhoods (csv)
            #===================================================================
            d['neighborhoods'] = self._try_find(soup,'span',{'class':'neighborhood-str-list'},strip=True)
            #===================================================================
            # address info, street, city, state_code,zip_code
            #===================================================================
            a = self._try_find(soup,'address',separator=',',strip=True)
            b = a.find(',')
            c = a.rfind(',')
            d['address'] = a[:b]
            d['city'] = a[b+1:c]
            d['state_code'] = a[c+2:c+4]
            d['postal_code'] = a[c+5:c+10]
            del a,b,c
            
            yield d
    
    #===========================================================================
    # get the more detailed info about a store (2nd level)
    #===========================================================================
    def get_store_details(self,store_yelp_url):
        """
        the details page for a store
        returns a dict of new fields...
        """
        return dict()
    
    #===========================================================================
    # load url
    #===========================================================================
    def _load_url(self,**keys):
        """
        make sure you're passing all the keys for self._base_url!
        """
        # make request
        r = self._session.get(self._base_url.format(**keys))
        self.logger.debug('url=%s',r.url)
        return r
    
    #===========================================================================
    # get soup from response (make request into soup!)
    #===========================================================================
    def _get_soup_from_response(self,response):
        """
        make sure the response hasn't already been read...
        """
        # turn search response into soup and return it!
        return BeautifulSoup(response.content)#.encode('UTF-8'))
    
    #===========================================================================
    # get soup from search
    #===========================================================================
    def _get_soup_from_search(self,**keys):
        """
        does both - loads the search, and turns it into beautiful soup.
        """
        return self._get_soup_from_response(self._load_url(**keys))
    
    #===========================================================================
    # Try Find
    #===========================================================================
    def _try_find(self,soup,tag,d={},use_get=True,**keys):
        """
        helper function...
        """
        a = soup.find(tag,d)
        if a is None: return ''
        elif use_get: return a.get_text(**keys)
        else: return a
        
    #===========================================================================
    # Static Methods
    #===========================================================================
    @staticmethod
    def iter_cross_product(*seq):
        for i in seq[0]:
            if len(seq)>1:
                for v in YelpWebCrawler.iter_cross_product(*seq[1:]):
                    yield (i,)+v
            else: yield (i,)
    
    def _get_summary_info(self):
        a = MyLoggingBase._get_summary_info(self)
        a.extend(('stores: {:,}'.format(self._total_stores),
                  'stores missed: {:,}'.format(self._total_stores_missed),
                  'stores outside zip: {:,}'.format(self._stores_outside_zip)
                  ))
        return a


#===============================================================================
# Main 
#===============================================================================
if __name__ == '__main__':
    import main
    main.run_yelpwebcrawler()
    
    