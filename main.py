"""
Elias Wood (owns13927@yahoo.com)
2015-04-20
the script to start it off.
"""

def run_yelpwebcrawler():
    from yelpwebcrawler import YelpWebCrawler
    from myjson2csv import MyJSON2CSV
    
    places = (dict(state='CA',city='Los_Angeles'),)
    
    alcohols = ('Alcohol.beer_and_wine','Alcohol.full_bar')
    
    prices = ('RestaurantsPriceRange2.1','RestaurantsPriceRange2.2',
              'RestaurantsPriceRange2.3','RestaurantsPriceRange2.4')
    
    subcats = ('adultentertainment','airportlounges','champagne_bars',
               'cocktailbars','divebars','gaybars','hookah_bars','irish_pubs',
               'lounges','pubs','sportsbars','wine_bars','beergardens',
               'comedyclubs','countrydancehalls','danceclubs','jazzandblues',
               'karaoke','musicvenues','pianobars','poolhalls','afghani',
               'african','newamerican','tradamerican','arabian','argentine',
               'armenian','asianfusion','australian','austrian','bangladeshi',
               'bbq','basque','belgian','brasseries','brazilian',
               'breakfast_brunch','british','buffets','burgers','burmese',
               'cafes','cafeteria','cajun','cambodian','caribbean','catalan',
               'cheesesteaks','chickenshop','chicken_wings','chinese',
               'comfortfood','creperies','cuban','czech','delis','diners',
               'ethiopian','hotdogs','filipino','fishnchips','fondue',
               'food_court','foodstands','french','gastropubs','german',
               'gluten_free','greek','halal','hawaiian','himalayan','hotdog',
               'hotpot','hungarian','iberian','indpak','indonesian','irish',
               'italian','japanese','korean','kosher','laotian','latin',
               'raw_food','malaysian','mediterranean','mexican','mideastern',
               'modern_european','mongolian','moroccan','pakistani','persian',
               'peruvian','pizza','polish','portuguese','poutineries','russian',
               'salad','sandwiches','scandinavian','scottish','seafood',
               'singaporean','slovakian','soulfood','soup','southern','spanish',
               'srilankan','steak','sushi','taiwanese','tapas',
               'tapasmallplates','tex-mex','thai','turkish','ukrainian','uzbek',
               'vegan','vegetarian','vietnamese')
    
    #===========================================================================
    # create the initial variables
    #===========================================================================
    a = YelpWebCrawler()
    a._init_logging()
    
    w = MyJSON2CSV(a.get_output_fd('test.csv'))
    w.set_headers('s_state','s_city','s_category','s_alcohol','s_price',
                  *a.get_default_headers())
    
    #===========================================================================
    # go through each combination
    #===========================================================================
    for place,cat,alcohol,price in YelpWebCrawler.iter_cross_product(places,
                                                                     subcats,
                                                                     alcohols,
                                                                     prices):
        # fields needed for each store
        search_d=dict(s_state=place['state'],
                      s_city=place['city'],
                      s_category=cat,
                      s_alcohol=alcohol,
                      s_price=price)
        
        #=======================================================================
        # iterate over each store
        #=======================================================================
        for result in a.iter_search(place['state'],place['city'],
                                    cat,
                                    attrs=','.join((alcohol,price))):
            # update with the search info
            result.update(search_d)
            # write to the csv
            w.write_json_object(result)
            
        # for testing, stop after at least 100
        if w._cur_row_num >100:
            break
        
    #===========================================================================
    # close writing
    #===========================================================================
    w.close()
    
    #===========================================================================
    # output summary info
    #===========================================================================
    w._log_summary_info()
    a._log_summary_info()
    


if __name__ == '__main__':
    run_yelpwebcrawler()
    
    
    
    